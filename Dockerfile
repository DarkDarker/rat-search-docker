FROM node

RUN apt-get update && apt-get install -y openvpn easy-rsa

RUN wget https://github.com/DEgITx/rats-search/archive/master.zip && \
    unzip master.zip && \
    rm -rf master.zip && \ 
    cd rats-search-master && \
    npm install && \
    npm run buildweb
    
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh   

HEALTHCHECK CMD curl --fail -s http://localhost:8095 || exit 1    

WORKDIR /rats-search-master

ENTRYPOINT ["/entrypoint.sh"]
CMD ["npm", "run", "server"]
