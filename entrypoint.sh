#!/bin/bash

set -e

if [[ -z "${OVPN_CONFIG}" ]]; then
  echo 'Starting without openvpn ...'  
else
  openvpn --keepalive 10 60 --client --config $OVPN_CONFIG &
  sleep 10
fi

# This will exec the CMD from your Dockerfile, i.e. "npm start"
exec "$@"
